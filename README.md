# Trái phiếu ủy thác thế chấp Collateral Trust Bond là gì

Rất nhiều doanh nghiệp và nhà đầu tư khi tham gia thị trường chứng khoán vẫn còn mông lung, chưa rõ trái phiếu thế chấp là gì, có đặc điểm như thế nào. Vì vậy thường bỏ qua các cơ hội tạo nguồn vốn và thu nhập mới cho mình. 